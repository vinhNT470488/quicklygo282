import SideMenu from "../pages/SideMenu";
import { Outlet } from "react-router";
function Layout() {
    return (
        <div>
            <SideMenu />
            <Outlet />
        </div>
    );
}
export default Layout;