import http from "./http-common";

const getAll = () => {
    return http.get("/Products");
  };  
  const get = (id:number) => {
    return http.get(`/Products/${id}`);
  };
  
  const create = (data:any) => {
    return http.post("/Products", data);
  };
  
  const update = (id: number, data:any) => {
    return http.put(`/Products/${id}`, data);
  };
  
  const remove = (id:number) => {
    return http.delete(`/Products/${id}`);
  };
  
  const removeAll = () => {
    return http.delete(`/Products`);
  };
  const ProductService = {
    getAll,
    get,
    create,
    update,
    remove,
    removeAll
  };
  
  export default ProductService;
//   const findByTitle = title => {
//     return http.get(`/Products?title=${title}`);
//   };
  