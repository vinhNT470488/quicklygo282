import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Layout  from "../views/Layout";
import Profile from "../pages/Profile";
import Chats from "../pages/Chats";
import Settings from "../pages/Settings";
import Contacts from "../pages/Contact";
const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        children: [
            {
                path: "/profile",
                element: <Profile />,
            },
            {
                path: "/chats",
                element: <Chats />,
            },
            {
                path: "/settings",
                element: <Settings />,
            },
            {
                path: "/contacts",
                element: <Contacts />,
            },
            // {
            //     path: "*",
            //     element: <NotFound />,
            // },
        ],
    },
]);
export default router;