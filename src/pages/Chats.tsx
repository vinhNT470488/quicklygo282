import React, { useState, useEffect } from "react";
import "../styles/tab-content.scss";
import "../styles/button-add.scss";
import BoxSearch from "../components/BoxSearch";
import BtnPlusAdd from "../components/btnPlusAdd";
import imgUser from "../assets/images/avatar-3.jpg";
import InboxContact from "../components/inboxContact";

function Chats() {
  useEffect(() => {
    const elements = document.querySelectorAll(".tab-content__inbox-contact");

    elements.forEach((element) => {
      (element as HTMLElement).addEventListener("click", (event) => {
        elements.forEach((el) => {
          if (
            (el as HTMLElement).classList.contains(
              "tab-content__inbox-contact--effect"
            )
          ) {
            (el as HTMLElement).classList.remove(
              "tab-content__inbox-contact--effect"
            );
          }
        });
        (element as HTMLElement).classList.add(
          "tab-content__inbox-contact--effect"
        );
      });
    });
  });

  return (
    <>
      <div className="tab-content">
        <div className="tab-content__header">
          <div className="tab-content__flex tab-content__flex--header-title">
            <div className="tab-content__title">
              <h4>Chats</h4>
            </div>
            <BtnPlusAdd />
          </div>
          <BoxSearch />
        </div>
        <div className="tab-content__body">
          <div className="tab-content__simple-bar">
            {/* <div className="tab-content__simple-bar-header">
              <div className="tab-content__simple-bar-title">
                <h5>favorites</h5>
              </div>
              <BtnPlusAdd />
            </div> */}
            <div className="tab-content__simple-bar-body">
              <ul>
                <li className="tab-content__inbox-contact">
                  <InboxContact />
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Chats;
