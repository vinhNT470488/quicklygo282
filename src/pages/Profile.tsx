import React, { useState, useEffect } from "react";
import "../styles/tab-content.scss";
import "../styles/button-add.scss";
import BoxSearch from "../components/BoxSearch";
import BtnPlusAdd from "../components/btnPlusAdd";
import imgUser from "../assets/images/avatar-3.jpg";

function Profile() {
  return (
    <>
       <div className="tab-content">
        <div className="tab-content__header">
          <div className="tab-content__flex tab-content__flex--header-title">
            <div className="tab-content__title">
              <h4>Profile</h4>
            </div>
          </div>
        </div>
        <div className="tab-content__body">
          <div className="tab-content__simple-bar">
            {/* <div className="tab-content__simple-bar-header">
              <div className="tab-content__simple-bar-title">
                <h5>favorites</h5>
              </div>
              <BtnPlusAdd />
            </div> */}
            <div className="tab-content__simple-bar-body">
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Profile;
