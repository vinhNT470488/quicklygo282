// actions/postActions.ts

import { FETCH_POSTS } from "./type";

export interface Post {
  id: number;
  title: string;
}

export const fetchPosts = (posts: Post[]) => ({
  type: FETCH_POSTS as typeof FETCH_POSTS,
  payload: posts,
});
