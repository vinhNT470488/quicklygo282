import { FETCH_USERS, FETCH_POSTS } from './type';

export interface User {
  id: number;
  name: string;
}

export const fetchUsers = (users: User[]) => ({
  type: FETCH_USERS as typeof FETCH_USERS,
  payload: users,
});
