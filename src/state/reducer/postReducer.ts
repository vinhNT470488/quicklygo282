// slices/postSlice.ts
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
export interface Post {
  id: number;
  title: string;
}
interface PostState {
  posts: Post[];
}

const initialState: PostState = {
  posts: [],
};
export const fetchPosts = createAsyncThunk('posts/fetchPosts', async () => {
    const response = await axios.get('https://jsonplaceholder.typicode.com/posts');
    console.log(response.data);
    return response.data;
  });

const postSlice = createSlice({
  name: 'post',
  initialState,
  reducers: {
    // fetchPosts: (state, action: PayloadAction<Post[]>) => {
    //   state.posts = action.payload;
    // },
    addPost: (state, action: PayloadAction<Post>) => {
      state.posts.push(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchPosts.fulfilled, (state, action) => {
      state.posts = action.payload;
    });
  },
});

export const { addPost } = postSlice.actions;
export default postSlice.reducer;


//react - typesript
//redux tookit
//axios
//tailwindui
// border: 1px  
// class="bor-1"
//headlessui
//ant