// slices/userSlice.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export interface User {
  id: number;
  name: string;
}

interface UserState {
  users: User[];
}

const initialState: UserState = {
  users: [],
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    fetchUsers: (state, action: PayloadAction<User[]>) => {
      state.users = action.payload;
    },
    addUser: (state, action: PayloadAction<User>) => {
      state.users.push(action.payload);
    },
  },
});

export const { fetchUsers, addUser } = userSlice.actions;
export default userSlice.reducer;
