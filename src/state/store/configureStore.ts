// store/configureStore.ts
import { configureStore } from '@reduxjs/toolkit';
import userReducer from '../reducer/userReducer';
import postReducer from '../reducer/postReducer';
import thunk from 'redux-thunk';
import rootReducer from '../reducer/rootReducer';

const store = configureStore({
  reducer: rootReducer,
});

export default store;
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
