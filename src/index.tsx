import React from "react";
import ReactDOM from "react-dom/client";
import "./styles/index.scss";
// import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "./styles/index.scss";
import Product from "./components/product";
import store from "./state/store/configureStore";
import { Provider } from "react-redux";
import "flowbite";
import { Modal } from "flowbite";
import type { ModalOptions, ModalInterface } from "flowbite";
import SideMenu from "./pages/SideMenu";
import Chats from "./pages/Chats";
import { RouterProvider } from "react-router-dom";
import router from "./router/router";
const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <div className="wrapper">
        <RouterProvider router={router} />
      </div>
    </React.StrictMode>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
