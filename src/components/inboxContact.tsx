import React, { useState, useEffect } from "react";
import "../styles/inbox-contact.scss";
import imgUser from "../assets/images/avatar-3.jpg";
function InboxContact() {
  return (
    <>
      <div className="inbox-contact__wrapper">
        <div className="inbox-contact__inbox-user-img">
          <img src={imgUser} alt="" />
        </div>
        <div className="inbox-contact__inbox-content">
          <div className="inbox-contact__inbox-flex">
            <div className="inbox-contact__inbox-name">
              <p>Nicholas Staten</p>
            </div>
            <div className="inbox-contact__inbox-time">
              <p>11 ago</p>
            </div>
          </div>
          <div className="inbox-contact__inbox-flex">
            <div className="inbox-contact__inbox-last-messages">
              <p>
                Hey, I'm going to meet a friend of mine at the department store.
                I have to buy some presents for my parents
              </p>
            </div>
            <div className="inbox-contact__inbox-status">
              <p>New</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default InboxContact;
