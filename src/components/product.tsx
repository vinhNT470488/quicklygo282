import React, { useState, useEffect } from "react";
import ProductDataService from "../services/ProductServices";
function Product() {
    const [Products, setProducts] = useState([]);
    const retrieveProducts = () => {
        ProductDataService.getAll()
          .then(response => {
            setProducts(response.data);
            console.log(response.data);
          })
          .catch(e => {
            console.log(e);
          });
      };
      useEffect(() => {
        retrieveProducts();
      }, []);
  return (
    <>
        this is list product
    </>
  );
}

export default Product;
