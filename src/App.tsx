// App.tsx
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from './state/store/configureStore';
import { User, addUser, fetchUsers } from './state/reducer/userReducer';
import { Post, fetchPosts } from './state/reducer/postReducer';
import type { AppDispatch } from './state/store/configureStore'; 
import { useAppDispatch } from './state/hooks';

const App: React.FC = () => {
  const dispatch = useAppDispatch();
  const users = useSelector((state: RootState) => state.user.users);
  const posts = useSelector((state: RootState) => state.post.posts);

  useEffect(() => {
    // Simulate fetching data from an API
    const mockUsers: User[] = [{ id: 1, name: 'John' }, { id: 2, name: 'Jane' }];
    const mockPosts: Post[] = [{ id: 1, title: 'Post 1' }, { id: 2, title: 'Post 2' }];
    dispatch(fetchUsers(mockUsers));  
    dispatch(fetchPosts());
  }, [dispatch]);
  //create a function to handle the click event`
  const handleClick = () => {
    const user: User = { id: 100, name: 'alex' };
    dispatch(addUser(user));
  };
  return (
    <div>
      <h1 className="text-4xl font-extrabold dark:text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto libero officiis ipsum quae unde. Autem eaque, hic animi aliquid impedit dolorem nisi numquam quisquam consectetur corrupti aperiam voluptatum repudiandae facilis.</h1>
      <button onClick={handleClick}>click demo</button>
      <h1>Users</h1>
      <ul>
        {users.map((user) => (
          <li key={user.id}>{user.name}</li>
        ))}
      </ul>
      <h1>Posts</h1>
      <ul>
        {posts.map((post) => (
          <li key={post.id}>{post.title}</li>
        ))}
      </ul>
    </div>
  );
};

export default App;
